- name: Add influxdb signing key
  apt_key:
    url: "https://repos.influxdata.com/influxdb.key"
    state: present

- name: Add influxdb deb repository
  apt_repository:
    repo: deb https://repos.influxdata.com/debian buster stable
    state: present

- name: Add grafana signing key
  apt_key:
    url: "https://packages.grafana.com/gpg.key"
    state: present

- name: Add grafana deb repository
  apt_repository:
    repo: deb https://packages.grafana.com/oss/deb stable main
    state: present

- name: Install monitoring tools
  apt:
    name:
      - smartmontools
      - sysstat
      - telegraf
      - munin
      - influxdb
      - grafana
    state: present

- name: Create munin HTML dir
  file:
    path: /var/www/admin/htdocs/munin
    state: directory
    owner: munin
    group: www-data
    mode: 0775

- name: Copy munin configs
  copy:
    src: munin.conf
    dest: /etc/munin/munin.conf

- name: Enable munin plugins
  file:
    src: "/usr/share/munin/plugins/{{ item }}"
    dest: "/etc/munin/plugins/{{ item }}"
    state: link
  with_items:
    - mysql_innodb
    - mysql_queries
    - mysql_slowqueries
    - mysql_threads
    - nginx_request
    - nginx_status
    - postfix_mailstats

- name: Copy custom munin plugins
  copy:
    src: "{{ item }}"
    dest: /etc/munin/plugins/{{ item }}
    mode: 0755
  with_items:
    - nodejs-chat
    - fs-logged-in
    - fs-registered

- name: Enable parametrized munin plugins
  file:
    src: "/usr/share/munin/plugins/{{ item.plugin }}"
    dest: "/etc/munin/plugins/{{ item.plugin }}{{ item.param }}"
    state: link
  with_items:
    - { plugin: 'mysql_', param: 'commands' }
    - { plugin: 'mysql_', param: 'connections' }
    - { plugin: 'mysql_', param: 'files_tables' }
    - { plugin: 'mysql_', param: 'innodb_io' }
    - { plugin: 'mysql_', param: 'innodb_io_pend' }
    - { plugin: 'mysql_', param: 'innodb_log' }
    - { plugin: 'mysql_', param: 'innodb_rows' }
    - { plugin: 'mysql_', param: 'innodb_tnx' }
    - { plugin: 'mysql_', param: 'innodb_semaphores' }
    - { plugin: 'mysql_', param: 'qcache' }
    - { plugin: 'mysql_', param: 'select_types' }
    - { plugin: 'mysql_', param: 'sorts' }
    - { plugin: 'mysql_', param: 'table_locks' }
    - { plugin: 'mysql_', param: 'tmp_tables' }
    - { plugin: 'sensors_', param: 'temp' }
    - { plugin: 'smart_', param: 'sda' }
    - { plugin: 'smart_', param: 'sdb' }
    - { plugin: 'smart_', param: 'sdc' }
    - { plugin: 'smart_', param: 'sdd' }

- name: Reload munin
  service:
    name: munin-node
    state: restarted
    enabled: yes

- name: Set sysctl rmem values
  copy:
    src: "20-rmem.conf"
    dest: "/etc/sysctl.d/20-rmem.conf"
    mode: 0755

- name: Create nginx server for status site
  copy:
    src: nginx-status-site.conf
    dest: /etc/nginx/sites-available/nginx-status-site.conf
  notify: Restart nginx


- name: Enable server block for status site
  file:
    src: /etc/nginx/sites-available/nginx-status-site.conf
    dest: /etc/nginx/sites-enabled/nginx-status-site.conf
    state: link
  notify: Restart nginx

- name: Create /opt/scripts
  file:
    path: /opt/scripts
    state: directory

- name: Install socketstats tool
  copy:
    src: socketstats.sh
    dest: /opt/scripts/socketstats.sh
    mode: 0755

- name: Allow telegraf to use www-data sockets
  user:
    name: telegraf
    append: yes
    groups: www-data

- name: Install influxdb config
  template:
    src: influxdb.conf
    dest: /etc/influxdb/influxdb.conf
  notify: Restart influxdb

- name: Install telegraf config
  template:
    src: telegraf.conf
    dest: /etc/telegraf/telegraf.conf
  notify: Restart telegraf

- name: Enable telegraf
  service:
    name: telegraf
    state: started
    enabled: yes

- name: Enable influxdb
  service:
    name: influxdb
    state: started
    enabled: yes